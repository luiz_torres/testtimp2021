jQuery.sap.declare('app.config.endpoints');
jQuery.sap.require('core.config.BaseData');

Data.config = {
    folder: 'timp/adm/server/endpoint.xsjs',
    type: 'json',
    endpoints: {
        users:
        {
            listUsers: 'users/listUsers/',
            listAdvancedFilters: 'users/listAdvancedFilters/'
        }
    }
};
