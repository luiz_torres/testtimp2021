jQuery.sap.registerModulePath("app", "ui/app");
jQuery.sap.require('app.config.endpoints');
jQuery.sap.require("sap.ui.app.Application");

sap.ui.app.Application.extend("TEST", {
	config: null,
	init: function() {
		this.config = {
			indexRoute: 'library',
			routes: {
				library: {
					layout: 'app.views.layout.Layout',
					controller: 'app.controllers.library.Library'
				}
			},
			components:{}
		};
	},
	main: function() {
		allTaxApp.init(this.config);
	}
});