sap.ui.controller("app.controllers.library.AccordionLayoutMessages", {
	onAfterRendering: function(html) {
		var _self = this;
		_self.view = html;
		_self.view.accordion = _self.view;
		_self.renderAccordion();
	},
	renderAccordion: function() {
		var _self = this;
		_self.view.accordion.empty();
		_self.view.accordion.ctrl = _self.view.accordion.bindBaseAccordion({
			accordion: [
				{
					customTitle: {
						name: 'core.views.TimpLibrary.libraryAccordionTitle',
						data: {
							title: "Usuário",
							iconFont: "User",
							icon: "User"
						}
					},
					identifier: "Users",
					tooltip: "Usuários",
					onPress: function() {
					}
                }
            ]
        })
    }
})