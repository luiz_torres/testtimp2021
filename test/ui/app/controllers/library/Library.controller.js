sap.ui.controller("app.controllers.library.Library", {
	onDataRefactor: function(data) {
		return $.extend(data, this.data);
	},
	onInit: function() {
        this._views = {	
			leftContent: {
				view: "app.views.library.LeftContent",
				wrapper: "#left-content"
			},
			rightContent: {
				view: "app.views.library.rightContent",
				wrapper: "#right-content"
			}
		};
		$('title').text(escapeHtml("Exercício"));
		$('#main-title').text(escapeHtml(i18n('USERS LIBRARY')));
	},
	onBeforeRendering: function(){
		
	},
	onAfterRendering: function(html) {
		var _self = this;
    	this.view = html;    	
	}

});