sap.ui.controller("app.controllers.library.LeftContent",{
    onInit: function() {},
    onBeforeRendering: function() {
        var controller = this;
    },
    onAfterRendering:function(html){
        var _self = this;
        this._view = $(html);
        this._view.tabs = this._view.find(".tabs-container");
        this.renderTabs();
    },
    
    renderTabs: function() {
        var _self = this;
        _self._view.tabs.ctrl = _self._view.tabs.bindBaseTabs({
            tab: [{
                viewName: "app.views.library.AccordionLayoutMessage",
                viewData: {},
                title: "Usuário",
                tooltip: {
                class: "dark",
                position: "right",
                width: 300,
            },
            identifier: 'Users'
            }],
            type: "boxes",
            wrapperClass: "wrapperClass"
        });
    }
});