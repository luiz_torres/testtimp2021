sap.ui.controller('app.controllers.library.rightContent', {
	onInit: function() {},
	onDataRefactor: function(data) {
        return $.extend(data, this.data);
	},
    onAfterRendering: function(html){
		var _self = this;
		_self.view = $(html);
		_self.coreServices.page = 1;
        _self.view.rightContent = _self.view.parent().parent();
		_self.view.toolBar = _self.view.find('.toolbar'); 
        _self.view.tilesPaginator = _self.view.find('.tiles-paginator'); 
        _self.view.table = _self.view.find('.table'); 
        _self.view.searchInput = _self.view.find('.search-input');
		_self.view.advancedFilters = _self.view.find('.library-advanced-filters-container');
		_self.view.advancedFiltersBtn = _self.view.find('#advanced-filters-btn');
		_self.view.resetBtn = _self.view.find('#library-filters #reset-filters-btn');
		_self.view.group = _self.view.find('#group');
		_self.view.createdBy = _self.view.find('#created-by');
		_self.view.creationDate = _self.view.find('#creation-date');
		_self.view.modifiedBy = _self.view.find('#modified-by');
		_self.view.modificationDate = _self.view.find('#modification-date');
		_self.view.status = _self.view.find('#status');

		_self.bindEvents();
		_self.bindElements();
    },
	bindElements: function() {
		var _self = this;
		_self.renderToolBar();
		_self.renderSearchInput();
		_self.renderFilters();
		_self.loadList();
	},
	bindEvents: function() {
		var _self = this;
		_self.view.advancedFiltersBtn.unbind('click').bind('click', function() {
			if (!_self.renderedFilters) {
				_self.view.advancedFilters.css("display", "inline-table");
				_self.view.table.animate({
					top: _self.view.advancedFilters.height() + _self.view.table.offset().top - 20
				}, 200);
				_self.view.advancedFiltersBtn.find('.button-icon').removeClass('icon-collapsedown');
				_self.view.advancedFiltersBtn.find('.button-icon').addClass('icon-collapseup');
			} else {
				_self.view.advancedFilters.slideUp();
				_self.view.table.animate({
					top: 120
				}, 100);
				_self.view.advancedFiltersBtn.find('.button-icon').removeClass('icon-collapseup');
				_self.view.advancedFiltersBtn.find('.button-icon').addClass('icon-collapsedown');
			}
		});
		_self.view.advancedFiltersBtn.baseTooltip({
			class: 'dark',
			position: 'top',
			text: "Filtros avançados"
		});
		_self.view.resetBtn.baseTooltip({
			class: 'dark',
			position: 'top',
			text: "Limpar filtros"
		});
		_self.view.resetBtn.unbind('click').bind('click', function() {
			_self.cleanFilters();
			_self.loadList();
		});

	},
	loadList: function() {
		var _self = this;
		var callback = function(response) {
			if (response && _.isArray(response.users)) {
				_self.renderTable(response);
				_self.renderPag(response.pageCount);
			} else {
				_self.renderTable({
					users: [],
					pageCount: 1
				});
			}
		};
		var filters = _self.getFilters();
		var objToPost = {
			page: _self.coreServices.page,
			filters: filters,
			hanaUser: []
		};
		Data.endpoints.users.listUsers.get(objToPost).success(callback).error(callback);
	},
	getFilters: function() {
		var _self = this;
		var filterBy = {};
		var hanaUser = _self.view.searchInput.ctrl.getValue();
		var group = _self.view.group.ctrl.getKey();
		var createdBy = _self.view.createdBy.ctrl.getKey();
		var creationDate = _self.view.creationDate.ctrl.getValue();
		var modifiedBy = _self.view.modifiedBy.ctrl.getKey();
		var modificationDate = _self.view.modificationDate.ctrl.getValue();
		var status = _self.view.status.ctrl.getKey();
		if (!_.isNil(hanaUser) && hanaUser !== "") {
			filterBy.hanaUser = hanaUser;
		}
		if (!_.isNil(group) && group !== "") {
			filterBy.group = group;
		}
		if (createdBy.length > 0) {
			filterBy.createdBy = createdBy;
		}
		if (!_.isNil(creationDate)) {
			filterBy.creationDate = [creationDate.hanaStartDate, creationDate.hanaEndDate];
		}
		if (modifiedBy.length > 0) {
			filterBy.modifiedBy = modifiedBy;
		}
		if (!_.isNil(modificationDate)) {
			filterBy.modificationDate = [modificationDate.hanaStartDate, modificationDate.hanaEndDate];
		}
		if (status.length > 0) {
			filterBy.status = status;
		}
		return filterBy;
	},
	cleanFilters: function() {
		_self = this;
		this.view.searchInput.ctrl.setValue("", true);
		this.view.group.ctrl.setKey(null, true);
		this.view.createdBy.ctrl.setKey(null, true);
		this.renderCreationDate();
		this.view.modifiedBy.ctrl.setKey(null, true);
		this.renderModificationDate();
		this.view.status.ctrl.setKey(null, true);
	},
	renderFilters: function() {
		var _self = this;
		_self.renderGroup({
			disabled: true
		});
		_self.renderCreatedBy({
			disabled: true
		});
		_self.renderCreationDate();
		_self.renderModifiedBy({
			disabled: true
		});
		_self.renderModificationDate();
		_self.renderStatus({
			disabled: true
		});
		var callback = function(response) {
			if (response.groups) {
				var groups = [{
					key: null,
					name: ''
				}];
				_.forEach(response.groups, function(e) {
					groups.push({
						key: e.id,
						name: e.name
					});
				});
				_self.renderGroup({
					data: groups
				});
			}
			if (response.createdBy) {
				var createdBy = [];
				_.forEach(response.createdBy, function(e) {
					createdBy.push({
						key: e.id,
						name: e.name + " " + e.lastName
					});
				});
				_self.renderCreatedBy({
					data: createdBy
				});
			}
			if (response.modifiedBy) {
				var modifiedBy = [];
				_.forEach(response.modifiedBy, function(e) {
					modifiedBy.push({
						key: e.id,
						name: e.name + " " + e.lastName
					});
				});
				_self.renderModifiedBy({
					data: modifiedBy
				});
			}
			if (response.status) {
				var statusOptions = [];
				_.forEach(response.status, function(st) {
					var name = "";
					if (st) {
						name = i18n(st.toUpperCase());
					}
					statusOptions.push({
						key: st,
						name: name
					});
				});
				_self.renderStatus({
					data: statusOptions
				});
			}
		};
		Data.endpoints.users.listAdvancedFilters.get().success(callback).error(callback);
	},
	renderGroup: function(options) {
		var _self = this;
		var json = {
			tooltip: "FILTRAR POR NOME DO GRUPO",
			placeholder: "Filtrar por nome do grupo",
			onChange: function(oldVal, newVal) {
				_self.loadList();
			}
		};
		if (_.isPlainObject(options)) {
			if (_.has(options, 'disabled')) {
				json.isDisabled = options.disabled;
			}
			if (_.has(options, 'data')) {
				json.options = options.data;
			}
		}
		_self.view.group.empty();
		_self.view.group.ctrl = _self.view.group.bindBaseSelect(json);
	},
	renderCreatedBy: function(options) {
		var _self = this;
		var json = {
			tooltip: "FILTRO POR USUÁRIO DE CRIAÇÃO",
			placeholder: "Filtrar por usuário de criação",
			onChange: function(oldVal, newVal) {
				_self.loadList();
			},
			isMultiple: true
		};
		if (_.isPlainObject(options)) {
			if (_.has(options, 'disabled')) {
				json.isDisabled = options.disabled;
			}
			if (_.has(options, 'data')) {
				json.options = options.data;
			}
		}
		_self.view.createdBy.empty();
		_self.view.createdBy.ctrl = _self.view.createdBy.bindBaseInputKat(json);
	},
	renderCreationDate: function(options) {
		var _self = this;
		var json = {
			placeholder: "Filtrar por data de criação",
			tooltip: "FILTRAR POR DATA DE CRIAÇÃO",
			onChange: function(oldVal, newVal) {
				_self.loadList();
			}
		};
		if (_.isPlainObject(options)) {
			if (_.has(options, 'disabled')) {
				json.isDisabled = options.disabled;
			}
		}
		_self.view.creationDate.empty();
		_self.view.creationDate.ctrl = _self.view.creationDate.bindBaseRangePicker(json);
	},
	renderModifiedBy: function(options) {
		var _self = this;
		var json = {
			tooltip: "FILTRO POR USUÁRIO DE MODIFICAÇÃO",
			placeholder: "Filtrar por usuário de modificação",
			onChange: function(oldVal, newVal) {
				_self.loadList();
			},
			isMultiple: true
		};
		if (_.isPlainObject(options)) {
			if (_.has(options, 'disabled')) {
				json.isDisabled = options.disabled;
			}
			if (_.has(options, 'data')) {
				json.options = options.data;
			}
		}
		_self.view.modifiedBy.empty();
		_self.view.modifiedBy.ctrl = _self.view.modifiedBy.bindBaseInputKat(json);
	},
	renderModificationDate: function(options) {
		var _self = this;
		var json = {
			placeholder: "Filtrar por data de modificação",
			tooltip: "FILTRAR POR DATA DE MODIFICAÇÃO",
			onChange: function(oldVal, newVal) {
				_self.loadList();
			}
		};
		if (_.isPlainObject(options)) {
			if (_.has(options, 'disabled')) {
				json.isDisabled = options.disabled;
			}
		}
		_self.view.modificationDate.empty();
		_self.view.modificationDate.ctrl = _self.view.modificationDate.bindBaseRangePicker(json);
	},
	renderStatus: function(options) {
		var _self = this;
		var json = {
			tooltip: "FILTRO POR STATUS",
			placeholder: "Filtrar por status",
			onChange: function(oldVal, newVal) {
				_self.loadList();
			},
			isMultiple: true
		};
		if (_.isPlainObject(options)) {
			if (_.has(options, 'disabled')) {
				json.isDisabled = options.disabled;
			}
			if (_.has(options, 'data')) {
				json.options = options.data;
			}
		}
		_self.view.status.empty();
		_self.view.status.ctrl = _self.view.status.bindBaseInputKat(json); 
	},
	renderSearchInput: function() {
		var _self = this;
		_self.view.searchInput.empty();
		_self.view.searchInput.ctrl = _self.view.searchInput.bindBaseInput({
			isSearchBox: true,
			tooltip: "Campo para pesquisa",
			onSearch: function(newVal) {
				if (_.isFinite(Number(newVal))) {
					_self._filters.id = Number(newVal);
				} else {
					_self._filters.adjustmentCode = newVal;
				}
				if (_self._filters.adjustmentCode === '') {
					delete _self._filters.adjustmentCode;
				}
				_self.loadList();
			}
		});
	},         
	renderToolBar: function(){
		var _self = this;
		_self.view.toolBar.empty();
		_self.view.toolBar = _self.view.toolBar.bindBaseLibraryToolbar({
			hideList: false,
			leftButtons: [{
				text: "Novo Usuário",
				icon: "plussign",
				iconFont: "Sign-and-Symbols",
				onPress: function(){
					console.log("Press");
				},
				enabled: true,
				tooltip: "Criar um novo usuário"
			},{
				icon: "dataset",
				iconFont: "DataManager",
				onPress: function(){
					console.log("Press");
				},
				enabled: true,
				'class': "example",
				tooltip: "Abrir ou fechar painel"
			},
			{
				icon: "useroK",
				iconFont: "User",
				onPress: function(){
					console.log("Press");
				},
				enabled: true,
				'class': "example",
				tooltip: "Ativar usuários"
			},
			{
				icon: "blockuser",
				iconFont: "User",
				onPress: function(){
					console.log("Press");
				},
				enabled: true,
				'class': "example",
				tooltip: "Desativar usuários"
			}]
		})
	},
	renderTable: function(data) {
			var _self = this;
			var body = _self.getTableBody(data.users);
			_self.view.table.empty();
			_self.view.table.ctrl = _self.view.table.bindBaseTable({
				hasActions: true,
				hasCheckboxes: true,
				hasShowHide: true,
				hasPagination: true,
				headers:[{
					text:"Name de Usuário",
					width:"100px",
					type:"text" //number,date,center
				},
				{
					text:"Usuário HANA",
					width:"100px",
					type:"text" //number,date,center
				},
				{
					text:"Email",
					width:"100px",
					type:"text" //number,date,center
				},{
					text:"Criado por",
					width:"100px",
					type:"text" //number,date,center
				},
				{
					text:"Data de Criação",
					width:"100px",
					type:"text" //number,date,center
				},
				{
					text:"Modificado por",
					width:"100px",
					type:"text" //number,date,center
				},
				{
					text:"Data de Modificação",
					width:"100px",
					type:"text" //number,date,center
				}
				],
				body: body,
				totalPages: data.pageCount,
				actualPage: _self.coreServices.page,
				onPageChange: function(oldVal, newVal) {
					_self.coreServices.page = newVal;
					_self.loadList();
				}
		});
	},
	getTableBody: function(data) {
		var _self = this;
		var body = [];
		_.forEach(data, function(element) {
			var userName = element.name + ' ' + element.lastName;
			var creationUser = "--";
			var modificationUser = "--";
			if (element.creationUser && element.creationUser.name) {
				creationUser = element.creationUser.name;
				if (element.creationUser.lastName) {
					creationUser += ' ' + element.creationUser.lastName;
				}
			}
			if (element.modificationUser && element.modificationUser.name) {
				modificationUser = element.modificationUser.name;
				if (element.modificationUser.lastName) {
					modificationUser += ' ' + element.modificationUser.lastName;
				}
			}
			body.push({
				actions: [],
				id: element.id,
				content: [
					userName,
					element.hanaUser,
					element.email,
					creationUser,
					parseDate(element.creationDate),
					modificationUser,
					parseDate(element.modificationDate)
				]
			});
		});
		return body;
	},
	renderPag: function(pageCount) {
		var _self = this;
		_self.view.tilesPaginator.empty();
		_self.view.tilesPaginator.ctrl = _self.view.tilesPaginator.bindBasePaginator({
			totalPages: pageCount,
			actualPage: _self.coreServices.page,
			onPageChange: function(oldVal, newVal) {
				_self.coreServices.page = newVal;
			}
		});
	},
});